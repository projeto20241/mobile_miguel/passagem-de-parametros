import React from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from 'react-native';

const TaskList = ({ navigation }) => {
    const tasks = [
        { id: 1, title: 'Comprar leite', date: '2024-02-27', time: '10:00', address: 'Supermercado XYZ' },
        { id: 2, title: 'Enviar e-mail', date: '2024-02-28', time: '14:30', address: 'Trabalho' },
        { id: 3, title: 'Estudar React Native', date: '2024-03-01', time: '09:00', address: 'Casa' }
    ];

    const handleTaskPress = (task) => {
        navigation.navigate('TaskDetail', { task });
    };

    const renderItem = ({ item }) => (
        <TouchableOpacity style={styles.taskItem} onPress={() => handleTaskPress(item)}>
            <Text style={styles.taskTitle}>{item.title}</Text>
        </TouchableOpacity>
    );

    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={styles.flatListContent}
                data={tasks}
                keyExtractor={(item) => item.id.toString()}
                renderItem={renderItem}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'bottom',
        alignItems: 'center',
    },
    flatListContent: {
        alignItems: 'center',
        width: '100%',
    },
    taskItem: {
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#8B4513',
        borderRadius: 5,
        width: '80%',
        alignItems: 'center',
    },
    taskTitle: {
        fontSize: 18, 
        color: '#FFFFFF', 
    },
});

export default TaskList;
