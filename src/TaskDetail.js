
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const TaskDetail = ({ route }) => {
    const { task } = route.params;

    return (
        <View style={styles.container}>
            <Text style={styles.taskItem}>Detalhes da Tarefa:</Text>
            <Text style={styles.taskItem}>Data: {task.date}</Text>
                        <Text style={styles.taskItem}>Hora: {task.time}</Text>
                        <Text style={styles.taskItem}>Local: {task.address}</Text>
        </View>
    );
    
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'bottom',
        alignItems: 'center',
    },
    flatListContent: {
        alignItems: 'center',
        width: '100%',
    },
    taskItem: {
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#8B4513',
        borderRadius: 5,
        width: '80%',
        alignItems: 'center',
    },
    taskTitle: {
        fontSize: 18, 
        color: '#FFFFFF', 
    },
});
export default TaskDetail;